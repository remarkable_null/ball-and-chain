﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayGameController : BaseController
{
	// 1: Generate balls that are bouncing around
	// 
	public UISprite     background;
	public bool         _alreadyAddedBall;

	#region Balls that bounce around the scene
	public Transform    ballContainer;  // Child GO transform where balls can all be contained to help keep hierarchy clean
	public List<Ball>   balls = null;

	private int         _ballsNeeded = 0;
	private int         _ballsInPlay = 0;
	private Vector3     _ballScale = Vector3.one * 20f;
	private float       _ballSpeed = 0.5f;
	#endregion


	/// <summary>
	/// Sets the number of balls in play.
	/// </summary>
	/// <param name="ballsNeeded">How many balls the player needs to hit to advance to the next level.</param>
	/// <param name="ballsInPlay">Total balls bouncing around this level.</param>
	public void SetBalls(int ballsNeeded, int ballsInPlay)
	{
		_ballsNeeded = ballsNeeded;
		_ballsInPlay = ballsInPlay;
		_alreadyAddedBall = false;

		UIEventListener.Get(background.gameObject).onClick += BackgroundClicked;
	}


	private void BackgroundClicked(GameObject go)
	{
		Debug.Log("BackgroundClicked: " + go.name);
		if (_alreadyAddedBall == false) {
			_alreadyAddedBall = true;
			ExplodingBall.Setup(0.5f, 2.25f, 0.25f, Vector3.one * 40f, Vector3.one * 100f, Vector3.one);

			float cameraPosZ = UICamera.currentCamera.transform.position.z;
			Debug.Log("Clicked " + UICamera.lastTouchPosition);
			Vector3 playerBallPos = new Vector3(UICamera.lastTouchPosition.x, UICamera.lastTouchPosition.y, -cameraPosZ);

			ExplodingBall.Explode(this.transform, playerBallPos, 0);
		}
	}


	public override void Initialize()
	{
		float halfScreenW = Screen.width * 0.5f - (Screen.width * 0.1f);    // Half the screen width, minus 10%
		float halfScreenH = Screen.height * 0.5f - (Screen.height * 0.1f);  // Half the screen height, minus 10%
		GameObject ballGOPrefab = Resources.Load("BallPrefab") as GameObject;
		if (ballGOPrefab == null) {
			Debug.LogError("ballGOPrefab is null");
			return;
		}

		balls = new List<Ball>(_ballsInPlay);
		for (int i = 0; i < _ballsInPlay; i++) {
			GameObject ballGO = Instantiate(ballGOPrefab) as GameObject;
			ballGO.name = "Ball" + i.ToString("00");
			Transform t = ballGO.transform;
			t.parent = ballContainer;
			t.localScale = _ballScale;
			
			Ball ball = ballGO.GetComponent<Ball>();
			if (ball != null) {
				ball.Initialize(new Vector2(halfScreenW, halfScreenH), _ballSpeed);
				balls.Add(ball);
			}
			else {
				Debug.LogError("ballGO.GetComponent<Ball> returned null.");
			}
		}
	}


	public override void Reset()
	{
		Debug.Log("PlayGameController.Reset()");
		// Clear away any balls
		if (balls != null) {
			foreach (Ball b in balls) {
				Destroy(b.gameObject);
			}
			balls.Clear();
		}
	}
}
