﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplodingBall : BallBase
{
	public delegate void OnCollision(Ball ball);
	public event OnCollision onCollision;

	// We need to know which number in the chain this ball is so we can award points correctly.
	// We also need to collide with any Ball object (ignoring other BallBase types) that hit us.
	// How long does the ball take to:
	// - Expand to full-size
	// - Stay full-size
	// - Shrink to nothing

	static private float    sScale1Time = 0f;
	static private float    sScale2Time = 0f;
	static private float    sScale3Time = 0f;
	static private Vector3  sScale1 = Vector3.zero;
	static private Vector3  sScale2 = Vector3.one;
	static private Vector3  sScale3 = Vector3.zero;
	static private bool     sInitialized = false;
	
	private int _numberInChain;


	/// <summary>
	/// Set up the global scaling times.
	/// </summary>
	/// <param name="time1">Time it takes go grow from its initial size to its largest size.</param>
	/// <param name="time2">Length of time to "stay" at its largest size.</param>
	/// <param name="time3">How quickly it shrinks down to size 0.</param>
	/// <param name="scale1">Starting scale, around the same size as the ball.</param>
	/// <param name="scale2">Maximum scaled size.</param>
	/// <param name="scale3">How small to make object scale down to before it gets destroyed.</param>
	static public void Setup(float time1, float time2, float time3, Vector3 scale1, Vector3 scale2, Vector3 scale3)
	{
		sScale1Time = time1;
		sScale2Time = time2;
		sScale3Time = time3;
		sScale1 = scale1;
		sScale2 = scale2;
		sScale3 = scale3;
		sInitialized = true;
	}


	static public void Explode(Transform parentTransform, Vector3 playerBallPos, int numberInChain)
	{
		// Create the ExplodingBall where the player clicked
		GameObject playerBall = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		Rigidbody rb = playerBall.AddComponent<Rigidbody>();
		rb.isKinematic = true;

		playerBall.name = "BallAsplooooooode" + numberInChain.ToString();
		playerBall.layer = LayerMask.NameToLayer("ExplosiveBall");
		playerBall.transform.parent = parentTransform;
		playerBall.transform.localScale = sScale1;
		playerBall.transform.position = UICamera.currentCamera.ScreenToWorldPoint(playerBallPos);

		ExplodingBall bp = playerBall.AddComponent<ExplodingBall>();
		bp.MakeItGoBOOM(numberInChain);
	}


	/// <summary>
	/// Give each instance its own chain number to know how to score it.
	/// </summary>
	/// <param name="numberInChain">Number in chain.</param>
	private void MakeItGoBOOM(int numberInChain)
	{
		if (sInitialized == true) {
			_numberInChain = numberInChain;
			Global.GetInstance().RegisterExplosion(this, _numberInChain);

			// Start the tweening
			TweenScale tween = TweenScale.Begin(gameObject, sScale1Time, sScale2);
			EventDelegate.Add(tween.onFinished, ScaleUpDone);
		}
	}


	/// <summary>
	/// Exploding ball grew in size from [initial] to [max] and we are now at max.
	/// </summary>
	private void ScaleUpDone()
	{
		Debug.Log("Scale up completed.");
		StartCoroutine(HoldScaleMax(sScale2Time));
	}


	private IEnumerator HoldScaleMax(float t)
	{
		yield return new WaitForSeconds(t);
		TweenScale tween = TweenScale.Begin(gameObject, sScale3Time, sScale3);
		EventDelegate.Add(tween.onFinished, ScaleDownDone);
	}


	private void ScaleDownDone()
	{
		Global.GetInstance().UnregisterExplosion(this);
		Destroy(gameObject);
	}


	private void OnCollisionEnter(Collision collision)
	{
		GameObject go = collision.collider.gameObject;
		if (go.GetComponent<Ball>() != null)
		{
			// Another ball has collided with this ExplodingBall
			Debug.Log("Collision between " + gameObject.name + " and " + go.name);
			go.SetActive(false);

			Transform t = collision.collider.transform;
			ExplodingBall.Explode(t.parent, UICamera.currentCamera.WorldToScreenPoint(t.position), _numberInChain + 1);

			//Destroy(go);
		}
	}
}
