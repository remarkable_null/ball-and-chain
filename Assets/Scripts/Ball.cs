﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
public class Ball : BallBase
{
	private bool  _initialized = false;
	private float _speed = 1f;


	public void Initialize(Vector2 halfScreen, float speed)
	{
		_speed = speed;

		// Give it an initial position.
		float positionX = Random.Range(-halfScreen.x, halfScreen.x);
		float positionY = Random.Range(-halfScreen.y, halfScreen.y);
		transform.localPosition = new Vector3(positionX, positionY, 0f);
		
		// Give it an initial push.
		float rotationZ = Random.Range(30f, 60f) + 90f * Random.Range(0,4);
		transform.eulerAngles = Vector3.forward * rotationZ;
		gameObject.rigidbody.AddForce(speed * transform.up, ForceMode.VelocityChange);
		
		//If the prefab doesn't have constraints pre-specified.  Simply set them here.
		//gameObject.rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

		_initialized = true;
	}
//
//
//	void OnCollisionEnter(Collision collision)
//	{
//		if (collision.collider.gameObject.GetComponent<ExplodingBall>() != null) {
//			Debug.Log("Collided with ExplodingBall.");
//		}
//		else if (collision.collider.gameObject.tag == "ExplosiveBall")
//		{
//			Debug.Log("Collided with ExplodingBall #2.");
//		}
//	}
}
