﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Global : MonoBehaviour
{
	public List<BaseController> gameControllers;  // Set in Inspector

	private List<ExplodingBall> _explodingBalls;    // Once it's filled and then cleared, end the level and evaluate goal
	private int _playerLevel;       // Increases after each goal is met
	private int _currentScore;      // Score for the current level, may not be permanent until goal is reached
	private int _totalScore;        // _currentScore is added to this when goal is reached after a level
	private int _ballsNeeded;       // Target goal
	private int _ballsCollected;    // Keep track of how many balls were collected during this level

	static BaseController       sActiveController = null;

	private enum GameState
	{
		Scene1_Instructions,
		Scene2_PlayGame,
		Scene2_PauseGame,
		Scene2_ResumeGame,
		Scene3_GameFailedRestart,
		Scene4_GameSucceededNextLevel,
		Scene5_ShareSocialMedia
	}
	private GameState _gameState;


	private enum GlobalState
	{
		Uninitialized,
		Ready,
		ShuttingDown
	}
	static private GlobalState  sGlobalState = GlobalState.Uninitialized;
	static private Global       sInstance    = null;


	static public Global GetInstance()
	{
		if (sGlobalState != GlobalState.ShuttingDown && sInstance == null) {
			GameObject instanceGO = GameObject.Find("Global");
			if (instanceGO == null) {
				instanceGO = new GameObject("Global");
				instanceGO.transform.localPosition = Vector3.zero;
			}
			sInstance = instanceGO.GetComponent<Global>();
			if (sInstance == null) {
				sInstance = instanceGO.AddComponent<Global>();
			}

			sGlobalState = GlobalState.Ready;
		}

		return sInstance;
	}


	#region Unity built-in methods
	void Start()
	{
		_gameState = GameState.Scene1_Instructions;
		_explodingBalls = new List<ExplodingBall>();
		_totalScore = 0;

		// Disable all other BaseControllers except PreGameController (set it active)
		foreach (BaseController bc in gameControllers) {
			PreGameController pgc = bc.GetComponent<PreGameController>();
			bc.gameObject.SetActive(pgc != null);
			if (pgc != null) {
				sActiveController = pgc;
				pgc.Reset();
				pgc.Initialize();
			}
		}
		_playerLevel = 0;
	}


	void OnDestroy()
	{
		sGlobalState = GlobalState.ShuttingDown;
		_explodingBalls.Clear();
		_explodingBalls = null;
	}
	#endregion


	#region Game states
	/// <summary>
	/// Start each level
	/// </summary>
	public void PlayGame()
	{
		if (_gameState != GameState.Scene2_PlayGame) {
			_gameState = GameState.Scene2_PlayGame;
			Debug.Log("Playing game level " + (_playerLevel + 1).ToString());

			// Reset for each new/replayed level
			_currentScore = 0;
			_ballsNeeded = 0;
			_ballsCollected = 0;

			// Disable all other BaseControllers except PlayGameController (set it active)
			foreach (BaseController bc in gameControllers) {
				PlayGameController pgc = bc.GetComponent<PlayGameController>();
				bc.gameObject.SetActive(pgc != null);
				if (pgc != null) {
					sActiveController = pgc;
					_ballsNeeded = (_playerLevel + 1) * 2 - 1;
					int ballsInPlay = (_playerLevel + 3) * 2 - 1;
					pgc.SetBalls(_ballsNeeded, ballsInPlay);
					pgc.Initialize();
				}
			}
		}
	}


	/// <summary>
	/// The game doesn't typically pause, so this should only get called when the player's session is interrupted.
	/// Phone call, switch away from the app, etc.
	/// </summary>
	public void PauseGame()
	{
		Debug.Log("Pausing game.");
	}


	/// <summary>
	/// Unpauses the game.
	/// </summary>
	public void UnpauseGame()
	{
		Debug.Log("Resuming paused game.");
	}


	public void EndGame()
	{
		Debug.Log("Stopping game.");
		sActiveController.Reset();

		// There will ALWAYS be 1 ball in _ballsCollected: the one the player drops. Don't count that one.
		if (_ballsCollected > 0) {
			_ballsCollected--;
		}

		if (_ballsCollected < _ballsNeeded) {
			// Player did not beat the level: Replay the last one
			_gameState = GameState.Scene3_GameFailedRestart;
			RestartLevel();
		}
		else {
			// Player beat the level: Go onto next level
			_gameState = GameState.Scene4_GameSucceededNextLevel;
			NextLevel();
		}
	}


	public void NextLevel()
	{
		_playerLevel++;
		Debug.Log("Advancing to next level " + (_playerLevel + 1).ToString());
		PlayGame();
	}


	public void RestartLevel()
	{
		Debug.Log("Restarting level " + (_playerLevel + 1).ToString());
		// Disable all other BaseControllers except RestartGameController (set it active)
		foreach (BaseController bc in gameControllers) {
			RestartGameController rgc = bc.GetComponent<RestartGameController>();
			bc.gameObject.SetActive(rgc != null);
			if (rgc != null) {
				sActiveController = rgc;
				rgc.ShowGoal(_ballsCollected, _ballsNeeded);
				rgc.Initialize();
			}
		}
	}
	#endregion


	public void AddScore(int chainNumber)
	{
		// ChainRxn uses '100 * [chainNumber]^3' for their scoring.
		_currentScore += 100 * (int)Mathf.Pow((float)chainNumber, 3f);
	}


	public void RegisterExplosion(ExplodingBall eball, int chainNumber)
	{
		_explodingBalls.Add(eball);
		AddScore(chainNumber);
		_ballsCollected++;
	}


	public void UnregisterExplosion(ExplodingBall eball)
	{
		if (_explodingBalls.Contains(eball)) {
			_explodingBalls.Remove(eball);
		}
		if (_explodingBalls.Count == 0) {
			EndGame();
		}
	}
}
