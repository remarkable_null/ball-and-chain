using UnityEngine;
using System.Collections;

// All controllers (specifically, ones attached to UIPanels) should derive from this base class.
public abstract class BaseController : MonoBehaviour
{
	abstract public void Initialize();
	abstract public void Reset();
}
