﻿using UnityEngine;
using System.Collections;

public class RestartGameController : BaseController
{
	public UISprite background;
	public UILabel title;

	public UILabel ballsFromLastPlay;
	public UILabel ballsNeededForGoal;

	private int _numBallsLastPlay = 0;
	private int _numBallsForGoal = 0;


	public void ShowGoal(int numBallsLastPlay, int numBallsForGoal)
	{
		_numBallsLastPlay = numBallsLastPlay;
		_numBallsForGoal = numBallsForGoal;

		if (ballsFromLastPlay != null) {
			ballsFromLastPlay.text = numBallsLastPlay.ToString();
		}
		if (ballsNeededForGoal != null) {
			ballsNeededForGoal.text = numBallsForGoal.ToString();
		}
	}


	public override void Initialize()
	{
		UIEventListener.Get(background.gameObject).onClick += StartReplayClicked;
	}


	private void StartReplayClicked(GameObject go)
	{
		Global.GetInstance().PlayGame();
	}


	public override void Reset()
	{
		Debug.Log("RestartGameController.Reset()");
	}
}
