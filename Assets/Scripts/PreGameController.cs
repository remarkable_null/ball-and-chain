﻿using UnityEngine;
using System.Collections;

public class PreGameController : BaseController
{
	#region Public Components: buttons, labels, etc.
	public UIButton playButton;
	#endregion


	#region Private objects, callbacks, scripts
	private Global _global;
	#endregion


	// Use this for initialization
	public override void Initialize()
	{
		Debug.Log("Started.");
		UIEventListener.Get(playButton.gameObject).onClick += OnPlayClicked;
	}


	public override void Reset()
	{
		// Nothing to reset on this controller
		Debug.Log("PreGameController.Reset()");
	}


	public void OnPlayClicked(GameObject go)
	{
		if (_global == null) {
			_global = Global.GetInstance();
		}
		_global.PlayGame();
	}
}
